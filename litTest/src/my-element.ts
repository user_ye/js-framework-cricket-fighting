import { LitElement, css, html } from 'lit'
import { customElement, property } from 'lit/decorators.js'
/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('my-element')
export class MyElement extends LitElement {
  /**
   * Copy for the read the docs hint.
   */
  @property()
  docsHint = 'Click on the Vite and Lit logos to learn more'

  @property({ type: Number })
  count = 100000

  @property({ type: Number })
  time = 0

  @property({ type: Boolean })
  viewFlag = true

  @property({ type: Boolean })
  show = true

  @property({ type: String })
  color = 'red'

  render() {
    return html`
    vite + Lit测试如下：
    显示：${this.show} &nbsp;&nbsp; len: ${this.count} &nbsp;&nbsp; time: ${this.time}ms
    <br/>
    <button @click="${this._onToggle}">显示/隐藏</button>&nbsp;&nbsp;
    <button @click="${() => this._onBg('red')}">红色</button>&nbsp;&nbsp;
    <button @click="${() => this._onBg('green')}">绿色</button>&nbsp;&nbsp;
    <hr/>
    <div style="width: 100%; max-height: 90vh; overflow: auto;">
      ${this.show && Array.from({length: this.count}, (_, i) => i).map(i => {
        if (i == this.count - 1) {
          this.viewFlag = false;
        }
        return html`
        <span id="box_${i}" class="box" style="background-color: ${this.color}"></span>
      `})}
    </div>
    `
  }

  private _onToggle() {
    this.show = !this.show

    if (!this.show) return ;
    this.viewFlag = true;
    let t = performance.now()
    let timer:any;
    timer = setInterval(() => {
      if (!this.viewFlag) {
        this.time = Math.floor(performance.now() - t)
        timer && clearInterval(timer);
      }
    }, 50);
  }

  private _onBg(c = 'red') {
    this.color = c;
    this.viewFlag = true;
    let t = performance.now()
    let timer:any;
    timer = setInterval(() => {
      if (!this.viewFlag) {
        this.time = Math.floor(performance.now() - t)
        timer && clearInterval(timer);
      }
    }, 50);
  }

  static styles = css`
    .box {
      width: 20px;
      height: 20px;
      display: inline-block;
      text-align: center;
      margin-right: 5px;
      margin-bottom: 5px;
      cursor: pointer;
    }
  `
}

declare global {
  interface HTMLElementTagNameMap {
    'my-element': MyElement
  }
}
