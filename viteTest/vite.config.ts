import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { Plugin as importToCDN, autoComplete } from 'vite-plugin-cdn-import'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(), 
    importToCDN({
      modules: [
        autoComplete('vue'),
      ],
    })
  ],
})
