import { useEffect, useState } from 'preact/hooks'

export function App() {
  // 200000
  const [count] = useState(100000)
  const [show, setShow] = useState(true)
  const [time, setTime] = useState(0)
  const [color, setColor] = useState('red')

  // 背景色
  const onBg = (c: string) => {
    setColor(c);
    const t = performance.now()
    const dom = document.getElementById('box_' + (count - 1))
    let timer: unknown|number = null;
    timer = setInterval(() => {
      if (dom?.style.backgroundColor == c){
        setTime(Math.floor(performance.now() - t))
        timer && clearInterval(timer as number);
      }
    }, 50);
    return ;
  }

  // 背景色
  const onTogle = () => {
    setShow(!show);
    if (!show) return ;
    const t = performance.now()
    let timer:unknown|number = null;
    timer = setInterval(() => {
      if (document.getElementById('box_' + (count - 1))){
        setTime(Math.floor(performance.now() - t))
        timer && clearInterval(timer as number);
      }
    }, 50);
    return ;
  }

  const t = performance.now()
  useEffect(() => {
    let timer:unknown|number = null;
    timer = setInterval(() => {
      if (document.getElementById('box_' + (count - 1))){
        setTime(Math.floor(performance.now() - t))
        timer && clearInterval(timer as number);
      }
    }, 50);
  }, [])

  return (
    <>
      <div>
        显示：{ show } &nbsp;&nbsp; len: { count } &nbsp;&nbsp; time: { time }ms
        <br/>
        <button onClick={() => onTogle()}>显示/隐藏</button>
        &nbsp;&nbsp;
        <button onClick={()=> onBg('red')}>红色</button>
        &nbsp;&nbsp;
        <button onClick={()=> onBg('green')}>绿色</button>
        &nbsp;&nbsp;
        <button >添加</button>
        <hr/>
        <div style={{width: "100%", maxHeight: "90vh", overflow: "auto"}}>
          {
            show && Array.from({ length: count }, (_, i) => (
              <span
                id={'box_' + i}
                key={i}
                style={{
                  width: '20px',
                  height: '20px',
                  background: color,
                  marginRight: '5px',
                  marginBottom: '5px',
                  display: 'inline-block',
                  textAlign: 'center',
                  cursor: 'pointer',
                }}
              />
            ))
          }
        </div>
      </div>
    </>
  )
}
