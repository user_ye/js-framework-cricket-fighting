import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
} from "recharts";
const t = `
Vue3	20	有时快有时慢	2200ms-3766ms	2382ms	2300mx	2359ms	2929ms	2282ms	有时快有时慢	550ms-1319ms	569ms	571ms	554ms	551ms	582ms
react18	5	较稳定 不会起伏大	9769ms-11000ms	11000ms	9532ms	9128ms	9378ms	9769ms	较稳定 不会起伏大	719ms-807ms	719ms	807ms	740ms	681ms	786ms
preact	10	较稳定 不会起伏大	3680ms-4496ms	4714ms	3524ms	3680ms	3719ms	4496ms	较稳定 不会起伏大	1810ms-2058ms	2058ms	1908ms	1922ms	1830ms	1810ms
solid-js	20	较稳定 不会起伏大	1035ms-1176ms	1176ms	1046ms	1039ms	1035ms	1055ms	较稳定 不会起伏大	353ms-423ms	353ms	357ms	417ms	358ms	423ms
lit	10	起伏有点小范围波动	1839-2283ms	2129ms	2190ms	2283ms	1839ms	2422ms	起伏有点小范围波动	606ms-755ms	606ms	755ms	730ms	594ms	652ms
vanila	20	稳定	761-826ms	826ms	756ms	764ms	762ms	761ms	稳定	249-297ms	264ms	254ms	251ms	297ms	249ms
svelte	20	起伏有点小范围波动	3679-4265ms	3903ms	3679ms	3865ms	4265ms	3607ms	稳定	1424-1537ms	1424ms	1428ms	251ms	1537ms	1355ms
qwik	5	起伏有点小范围波动	1404-1561ms	1404ms	1350ms	1419ms	1531ms	1561ms	稳定	28474-28600ms	28474ms	28231ms	28511ms	28464ms	28600ms
nuxt	20	稳定	2013ms-2456ms	2013ms	2270ms	2200ms	2456ms	2343ms	稳定	642ms-768ms	768ms	642ms	785ms	722ms	667ms
next	20	较稳定 不会起伏大	8991ms-10092ms	8991ms	10092ms	10137ms	9367ms	8928ms	稳定	663ms-772ms	747ms	725ms	785ms	772ms	663ms
ofa.js	5	稳定	2557ms-2654ms	2557ms	2654ms	2560ms	2539ms	2595ms 稳定	249-297ms	264ms	254ms	251ms	297ms	249ms
`;
const list: any = t
  .split("\n")
  .filter((item) => item.trim())
  .map((item) => item.split("\t"));
const domData: any = [];
const styleData: any = [];
for (let i = 4; i < 9; i++) {
  const name = "第" + (i - 3) + "次";
  const domItem = { name };
  const styleItem = { name };
  list.forEach((listItem: any) => {
    // @ts-ignore
    domItem[listItem[0]] = parseInt(listItem[i]);
    // @ts-ignore
    styleItem[listItem[0]] = parseInt(listItem[i + 6]);
  });
  domData.push(domItem);
  styleData.push(styleItem);
}

const width = window.innerWidth - 20;
const height = window.innerHeight / 2 - 30;

function App() {
  return (
    <div>
      <div style={{ position: "relative", paddingTop: 20 }}>
        <p style={{ position: "absolute", left: 70, top: -20 }}>dom渲染ms</p>
        <LineChart width={width} height={height} data={domData}>
          {list.map((item: any) => {
            const rgb = `rgb(${Math.random() * 255},${Math.random() * 255},${
              Math.random() * 255
            })`;
            return (
              <Line
                key={item[0]}
                type="monotone"
                dataKey={item[0]}
                stroke={rgb}
              />
            );
          })}
          <CartesianGrid stroke="#ccc" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
        </LineChart>
      </div>

      <div style={{ position: "relative", paddingTop: 20 }}>
        <p style={{ position: "absolute", left: 70, top: -20 }}>style渲染ms</p>
        <LineChart width={width} height={height} data={styleData}>
          {list.map((item: any) => {
            const rgb = `rgb(${Math.random() * 255},${Math.random() * 255},${
              Math.random() * 255
            })`;
            return (
              <Line
                key={item[0]}
                type="monotone"
                dataKey={item[0]}
                stroke={rgb}
              />
            );
          })}
          <CartesianGrid stroke="#ccc" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
        </LineChart>
      </div>
    </div>
  );
}

export default App;
