/*
 * Copyright (c) 2023 Huawei Technologies Co.,Ltd.
 *
 * openInula is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *          http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

import Inula, { useState } from 'openinula';
import './index.css';

function App() {
  const [count,setCount] = useState(50000)
  const [time,setTime] = useState(0)
  const [color,setColor] = useState('red')
  const [show,setShow] = useState(true)

   // 背景色
   const onBg = (c) => {
    setColor(c);
    const t = performance.now();
    const dom = document.getElementById("box_" + (count - 1));
    let timer;
    timer = setInterval(() => {
      if (dom?.style.backgroundColor == c) {
        setTime(Math.floor(performance.now() - t));
        timer && clearInterval(timer);
      }
    }, 50);
    return;
  };

  // 背景色
  const onTogle = () => {
    setShow(!show);
    if (!show) return;
    const t = performance.now();
    let timer;
    timer = setInterval(() => {
      if (document.getElementById("box_" + (count - 1))) {
        setTime(Math.floor(performance.now() - t));
        timer && clearInterval(timer);
      }
    }, 50);
    return;
  };


  return (
    <div class="container">
      显示：{show} &nbsp;&nbsp; len: {count} &nbsp;&nbsp; time: {time}ms
      <br />
      <button onClick={() => onTogle()}>显示/隐藏</button>
      &nbsp;&nbsp;
      <button onClick={() => onBg("red")}>红色</button>
      &nbsp;&nbsp;
      <button onClick={() => onBg("green")}>绿色</button>
      &nbsp;&nbsp;
      <button>添加</button>
      <hr />
       <div style={{ width: "100%", maxHeight: "90vh", overflow: "auto" }}>
          {show &&
            Array.from({ length: count }, (_, i) => (
              <span
                id={"box_" + i}
                key={i}
                style={{
                  width: "20px",
                  height: "20px",
                  background: color,
                  marginRight: "5px",
                  marginBottom: "5px",
                  display: "inline-block",
                  textAlign: "center",
                  cursor: "pointer",
                }}
              />
            ))}
        </div>
    </div>
  );
}

Inula.render(<App />, document.getElementById('root'));
