import { createSignal, onMount } from 'solid-js'
import { template } from 'solid-js/web'

function App() {
  // 200000 ref 
  const [count] = createSignal(200000)
  const [show, setShow] = createSignal(true)
  const [time, setTime] = createSignal(0)
  const [color, setColor] = createSignal('red')

  // 背景色
  const onBg = (c: string) => {
    setColor(c);
    const t = performance.now()
    const dom = document.getElementById('box_' + (count() - 1))
    let timer: unknown|number = null;
    timer = setInterval(() => {
      if (dom?.style.backgroundColor == c){
        setTime(Math.floor(performance.now() - t))
        timer && clearInterval(timer as number);
      }
    }, 50);
    return ;
  }

  // 背景色
  const onTogle = () => {
    setShow(!show());
    if (!show()) return ;
    const t = performance.now()
    let timer:unknown|number = null;
    timer = setInterval(() => {
      if (document.getElementById('box_' + (count() - 1))){
        setTime(Math.floor(performance.now() - t))
        timer && clearInterval(timer as number);
      }
    }, 50);
    return ;
  }

  onMount(() => {
    const t = performance.now()
    let timer:unknown|number = null;
    timer = setInterval(() => {
      if (document.getElementById('box_' + (count() - 1))){
        setTime(Math.floor(performance.now() - t))
        timer && clearInterval(timer as number);
      }
    }, 50);
  })
  
  return (
    <>
      <div>
        显示：{ show() } &nbsp;&nbsp; len: { count() } &nbsp;&nbsp; time: { time() }ms
        <br/>
        <button onClick={() => onTogle()}>显示/隐藏</button>
        &nbsp;&nbsp;
        <button onClick={()=> onBg('red')}>红色</button>
        &nbsp;&nbsp;
        <button onClick={()=> onBg('green')}>绿色</button>
        &nbsp;&nbsp;
        <button >添加</button>
        <hr/>
        <div style={{width: "100%", 'max-height': "90vh", overflow: "auto"}}>
          {
            show() && Array.from({ length: count() }, (_, i) => (
              <span
                id={'box_' + i}
                style={{
                  width: '20px',
                  height: '20px',
                  background: color(),
                  'margin-right': '5px',
                  'margin-bottom': '5px',
                  display: 'inline-block',
                  'text-align': 'center',
                  cursor: 'pointer',
                }}
                onMouseOut={(e) => {
                  e.currentTarget.style.background = color()
                }}
                onMouseOver={(e) => {
                  // 随机颜色
                  const r = Math.floor(Math.random() * 255)
                  const g = Math.floor(Math.random() * 255)
                  const b = Math.floor(Math.random() * 255)
                  const color = `rgb(${r},${g},${b})`
                  e.currentTarget.style.background = color
                }}
              />
            ))
          }
        </div>
      </div>
    </>
  )
}

export default App
