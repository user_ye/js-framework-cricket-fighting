## js 框架斗蟋蟀 代码已开源（可自行下载测试）

### **为了不占用大家时间 这里只演示：`preact/svelte/vue//solid`**

### 仅供娱乐 （真实使用场景 数据通信 组件封装 性能优化 生态 库 等多方面进行评测）

### 跑分如下：（打包后性能会提升 目前是运行环境测试的）

- vanillaTest 第一
- solid 第二
- vue3/nuxt 第三 (nuxt 操作 dom 比 vue3 快) 渲染样式比 vue3 `10%-15%` 慢百分之`10%`
- lit/svelte 第四

### ![image](./test.png)

### ![image](./test2.png)

### ![image](./test3.png)

### 性能不是唯一指标 （下面几个才是关键指标）

- 市场占有率 （方便找工作否）
- 生态、稳定、相关库、维护热度

### 新手还是首选 `vue3` `react` 生态起来了很难被挤掉

- 老手真到了需要考虑性能的时候 估计自己都能看源码去修改优化策略

### 安装使用

- 自行点击 `显示/切换` `红色` `绿色` 按钮 等待渲染时间

### 目录如下：

- `vanillaTest` 纯 原生 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `solid` solid-js 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `viteTest` vue3 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `litTest` lit 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `reactTest` react 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `preactTest` preact 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `qwikTest` qwik(ssr) 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `svelteTest` svelte 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `nuxtTest` nuxt(ssr) 性能测试

```
# 按照
npm i
# 运行
npm run dev
```

- `nextTest` next(ssr) 性能测试

```
# 按照
npm i
# 运行
npm run dev
```

- `ofaTest` ofa.js 性能测试

```
# 按照
pnpm i
# 运行
pnpm run dev
```

- `openlnulaTest` 华为开源框架 性能测试
  - 操作 css 优秀 操作 dom 比 react 还慢

```
# 按照
npm i
# 运行
npm run start
```
